const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const dotenv = require("dotenv");
dotenv.config();

const PORT = process.env.PORT;

// Database
const db = require("./models");
db.sequelize.sync();

const app = express();
var corsOptions = {
	origin: "http://localhost:8081",
};
app.use(cors(corsOptions));
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Routes
app.use("/", require("./controllers/index"));

const server = app.listen(PORT, () => {
	console.log(`App listening on port ${PORT}...`);
});

module.exports = server;
