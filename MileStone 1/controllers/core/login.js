const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const Joi = require("joi");

router.post("/", async (req, res) => {
	const { error } = validateUser(req.body);

	if (error) {
		return res.status(400).send(error.details[0].message);
	}

	const { email, password } = req.body;

	const token = jwt.sign({ _id: email }, process.env.JWT_PRIVATE_KEY, {
		expiresIn: "60m",
	});

	res.status(200).json({
		message: "Login Successful",
		jwtoken: token,
	});
});

function validateUser(user) {
	const schema = {
		email: Joi.string().email().required(),
		password: Joi.string().required(),
	};

	return Joi.validate(user, schema);
}

module.exports = router;
