const express = require("express");
const router = express.Router();

router.use("/login", require("./core/login"));

module.exports = router;
